{ mkDerivation, base, blaze-html, bytestring, conduit, containers
, hspec, HUnit, monad-control, monad-logger, persistent
, persistent-sqlite, persistent-template, QuickCheck, resourcet
, stdenv, tagged, text, time, transformers, unordered-containers
}:
mkDerivation {
  pname = "esqueleto";
  version = "2.5.2.1";
  src = ./.;
  libraryHaskellDepends = [
    base blaze-html bytestring conduit monad-logger persistent
    resourcet tagged text time transformers unordered-containers
  ];
  testHaskellDepends = [
    base conduit containers hspec HUnit monad-control monad-logger
    persistent persistent-sqlite persistent-template QuickCheck
    resourcet text time transformers
  ];
  homepage = "https://github.com/bitemyapp/esqueleto";
  description = "Type-safe EDSL for SQL queries on persistent backends";
  license = stdenv.lib.licenses.bsd3;
}
